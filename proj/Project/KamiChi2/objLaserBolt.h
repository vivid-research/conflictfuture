#pragma once
#include "kObj.h"
#include "kSoundSource.h"
#include "kSound.h"
#include "kScene.h"
class objLaserBolt : public kObj
{
public:

	static kSoundSource* lasSrc;
	static kImage* imgSrc;

	objLaserBolt(kamGame *g);

	void update() {

		move(25, 0);

	};

	void render() {

		getGame()->drawImg(getX()-getW()/2, getY()-getH()/2, getW(), getH(), 1, 1, 1, 0.3f, boltImg);
		//getOwner()->addWave(getX() + getW() / 2, getY() + getH() / 2, 2, 1.4f);
	};

private:

	kImage* boltImg;
	

};

