#include "kamCore.h"
#include <iostream>

void (*key_fun)(KEYS,bool);
void (*mouse_fun)(KEYS, bool);

bool cleft = false;
bool cright = false;
bool cup = false;
bool cdown = false;

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (xpos > 2) {
		if (!cright) {
			cright = true;
			if (cleft == true) {
				mouse_fun(KEYS::Left, false);
				cleft = false;
			}
			mouse_fun(KEYS::Right, true);
		}
	}
	if (xpos < -2) {
		if (!cleft) {
			cleft = true;
			if (cright == true) {
				mouse_fun(KEYS::Right, false);
				cright = false;
			}
			mouse_fun(KEYS::Left, true);
		}
	}

	if (ypos > 2) {
		if (!cdown) {
			cdown = true;
			if (cup == true) {
				mouse_fun(KEYS::Up, false);
				cup = false;
			}
			mouse_fun(KEYS::Down, true);
		}
	}
	if (xpos < -2) {
		if (!cup) {
			cup = true;
			if (cdown == true) {
				mouse_fun(KEYS::Down, false);
				cdown = false;
			}
			mouse_fun(KEYS::Up, true);
		}
	}
	if (xpos ==0) {
		cleft = false;
		cright = false;
		mouse_fun(KEYS::Left, false);
		mouse_fun(KEYS::Right, false);
	};
	if (ypos == 0) {

		cup = false;
		cdown = false;
		mouse_fun(KEYS::Up, false);
		mouse_fun(KEYS::Down, false);

	}
}

kamCore::kamCore(int w, int h)
{

	winW = w;
	winH = h;

	std::cout << "Initializing KamiChi core." << std::endl;
	std::cout << "Creating window. W:" << w << " H:" << h << std::endl;

	glfwInit();

	win = glfwCreateWindow(w, h, "KamiChi 2 - Return of the Saviour", NULL, NULL);

	if (!win) {

		glfwTerminate();
		return;

	}



	glfwMakeContextCurrent(win);

	glewInit();

	glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


}


void key_cb(GLFWwindow *win,int key,int scancode,int action,int mods) {

	
	if (action == GLFW_REPEAT) return;

	if (key == GLFW_KEY_LEFT || key == GLFW_KEY_A) {

		key_fun(KEYS::Left, action == GLFW_PRESS);

	}
	if (key == GLFW_KEY_ENTER || key == GLFW_KEY_SPACE || key == GLFW_KEY_LEFT_ALT || key == GLFW_KEY_RIGHT_ALT)
	{

		key_fun(KEYS::Action, action == GLFW_PRESS);
		//&key_fun(KEYS::Action);

	}

	

	if (key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) {
		key_fun(KEYS::Right, action == GLFW_PRESS);
	}
	if (key == GLFW_KEY_UP || key == GLFW_KEY_W) {

		key_fun(KEYS::Up,action == GLFW_PRESS);

	}
	if (key == GLFW_KEY_DOWN || key == GLFW_KEY_S) {

		key_fun(KEYS::Down,action == GLFW_PRESS);

	}

}

void kamCore::setMouseCallback(void (*m)(KEYS, bool))
{
	mouse_fun = m;
	glfwSetCursorPosCallback(win, cursor_position_callback);
}

void kamCore::setKeyCallback( void (*f)(KEYS,bool) ) {


	key_fun =f;

	glfwSetKeyCallback(win, key_cb);
	
}




void kamCore::updateCore() {

	glfwPollEvents();

}

void kamCore::beginRender() {

	glClearColor(0,0,0,0);

	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();

	gluOrtho2D(0, winW, winH, 0);



}

void kamCore::endRender() {

	glfwSwapBuffers(win);

}

int kamCore::getW() {

	return winW;
	
}

int kamCore::getH() {

	return winH;

}