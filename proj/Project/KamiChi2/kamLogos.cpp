#include "kamLogos.h"

kamLogos::kamLogos(kamGame* g,int num) {

	game = g;
	numLogo = num;
	logo.resize(num);
	fxLight = new kEffect("data/shader/light/lightvertex.glsl", "data/shader/light/lightfrag.glsl");


}

void kamLogos::setLogo(int i, kImage* img)
{

	logo[i] = img;
	
	img->x = game->getW() + 100 + (i * game->getW());
	img->y = game->getH() / 2;



}

bool kamLogos::done() {

	kImage* i = logo[(int)(numLogo - 1)];

	if (i->x<-150)
	{

		return true;
	}
	return false;
}

void kamLogos::update() {

	for (int i = 0; i < numLogo; i++) {

		kImage* s = logo[i];
		s->x = s->x - 4;

	

	}

}

void kamLogos::render() {

	game->setBlend(BLENDMODE::Alpha);

	float mx = game->getW() / 2;
	mx = mx - 128;

	fxLight->bind();

	fxLight->setInt("diffTex", 0);
	fxLight->setVec3("lightPos", game->getW() / 2, game->getH() / 2, 0);
	fxLight->setVec3("lightDiff", 0, 1, 1);
	fxLight->setFloat("lightRange", 450);

	for (int i = 0; i < numLogo; i++) {

		kImage* s = logo[i];

		float dis = abs(s->x - mx);



		//dis = game->getW() / 2;
		dis = dis / (float)(game->getW() / 2);

		dis = 1.0 - dis;
		if (dis < 0.03f) dis = 0.03f;

		game->drawImg(s->x, s->y, 256, 80, 1, 1, 1, dis, s, dis);

	}

	fxLight->unbind();

}