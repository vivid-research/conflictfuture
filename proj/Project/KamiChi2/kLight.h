#pragma once
#include "kObj.h"
class kLight : public kObj
{
public:

	kLight();

	void setColor(float cr, float cg, float cb);

	void setRange(float r);

	float getRange();

	float getDifR();
	float getDifG();
	float getDifB();

private:
	float difr, difg, difb;
	float range;
	

};

