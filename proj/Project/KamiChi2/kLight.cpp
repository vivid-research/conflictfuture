#include "kLight.h"

kLight::kLight() {

	difr = 1.0f;
	difg = 1.0f;
	difb = 1.0f;
	range = 800;
	
}

void kLight::setColor(float r, float g, float b) {

	difr = r;
	difg = g;
	difb = b;

};

void kLight::setRange(float r) {

	range = r;

}

float kLight::getRange() {

	return range;

};

float kLight::getDifR() {

	return difr;

}

float kLight::getDifG() {

	return difg;

}

float kLight::getDifB() {

	return difb;

}