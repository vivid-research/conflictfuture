#include "stateNormalMode.h"

stateNormalMode::stateNormalMode(kamCore * c,kamGame* g) {

	setGame(g);
	scene = new kScene(c,g);
	scene->setBg(new kImage("data/img/star/starbg1.png"));

	kLight* l = new kLight();

	l->setPos(200, 200,1);
	l->setRange(700);
	l->setColor(1, 1, 0.3f);

	kLight* l2 = new kLight();

	l2->setColor(0.3f, 1,1.0f);


	l2->setPos(600, 600, 1);
	l2->setRange(700);

	scene->addLight(l);
	scene->addLight(l2);


}

void stateNormalMode::init() {

	


}

void stateNormalMode::update() {

	if (hasMusic == true && musicStarted == false) {

		getGame()->playMusic(music);
		musicStarted = true;

	}


	scene->update();

}

void stateNormalMode::render() {

	
	scene->renderLit();

}

void stateNormalMode::done() {

}

void stateNormalMode::pause() {

}

void stateNormalMode::resume() {

}

void stateNormalMode::addObj(kObj* o)
{

	scene->addObj(o);

}
