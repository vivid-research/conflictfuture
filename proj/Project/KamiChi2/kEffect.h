#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

class kEffect
{
public:

	kEffect(const char* v, const char* f);
	void bind();
	void unbind();
	void setInt(const char* name, int val);
	void setFloat(const char* name, float val);
	void setVec3(const char* name, float v1, float v2, float v3);

private:

	GLuint vID, fID;
	GLuint pID;

};

