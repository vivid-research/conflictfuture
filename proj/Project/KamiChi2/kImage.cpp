#include "kImage.h"
#include <lodepng.h>
#include <vector>
#include <iostream>

GLuint kImage::getID() {

	return id;

}

kImage::kImage(int cw, int ch) {

	w = (int)cw;
	h = (int)ch;

	glGenTextures(1, &id);



	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, id);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);



}

kImage::kImage(unsigned char* data, int cw, int ch) {


	w = (int)cw;
	h = (int)ch;



	glGenTextures(1, &id);



	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, id);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);



}

kImage::kImage(const char* path) {

	iPath = path;

	std::vector<unsigned char> img;
	unsigned iw, ih;
	unsigned error = lodepng::decode(img, iw, ih, path);

	if (error != 0) {
		std::cout << "error " << error << ": " << lodepng_error_text(error) << std::endl;
		return;
	}

	std::cout << "Loaded image. W:" << iw << " H:" << ih << std::endl;

	w = (int)iw;
	h = (int)ih;

	

	glGenTextures(1, &id);

	
	
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, id);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, &img[0]);






}

void kImage::bind(int unit) {

	
	glActiveTexture(GL_TEXTURE0 + unit);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, id);

}

void kImage::release(int unit) {

	glActiveTexture(GL_TEXTURE0 + unit);
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

}