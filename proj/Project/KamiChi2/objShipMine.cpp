#include "objShipMine.h"

objShipMine::objShipMine() {

	mine = new kImage("data/img/obj/enemy/shipMine/mineidle1-game.png");
	setSize(128, 96);
	

};

void objShipMine::render() {

	if (getHp() <= 0) return;
	//exit(1);
	getGame()->drawImg(getX(), getY(), getW(), getH(), 1, 1, 1, 1, mine,1.0f,getAng());



}

void objShipMine::postRender() {

	if (sparks != NULL) {

		sparks->render();

	}

}