uniform sampler2D fb;
uniform float blur;


void main()
{

    vec2 uv = gl_TexCoord[0].st;
    uv.y = 1.0-uv.y;

    vec3 bc = vec3(0,0,0);

    float samples=0;


    for(float by=-5;by<5;by++){

        for(float bx=-5;bx<5;bx++){

            vec2 buv = uv;

            buv.x = buv.x + bx*blur*0.0065;
            buv.y = buv.y + by*blur*0.0065;

            vec3 c = texture2D(fb,buv).rgb;

            bc = bc + c;

            samples++;

        }

    }

    bc = bc / (samples * 0.7);

    vec4 texCol = vec4(bc.x,bc.y,bc.z,1.0);


    gl_FragColor = texCol;

}