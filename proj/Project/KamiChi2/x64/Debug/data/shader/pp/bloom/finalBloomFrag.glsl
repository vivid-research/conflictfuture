uniform sampler2D fb;
uniform sampler2D blur;



void main()
{

    vec2 uv = gl_TexCoord[0].st;
    uv.y = 1.0-uv.y;

    vec3 fc = texture2D(fb,uv).rgb;
    vec3 bv = texture2D(blur,uv).rgb;

    fc = fc * 0.65 + bv*0.35;
    
 
    gl_FragColor = vec4(fc.r,fc.g,fc.b,1.0);

}