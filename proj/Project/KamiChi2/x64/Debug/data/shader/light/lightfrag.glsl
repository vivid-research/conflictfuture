uniform sampler2D diffTex;

const float gW = 1300;
const float gH = 768;

uniform vec3 lightPos;
uniform vec3 lightDiff;
uniform float lightRange;

void main(){

    vec4 col = vec4(gl_FragCoord.x,gl_FragCoord.y,0,1);

    col.y = gH - col.y;

    float xD = lightPos.x - col.x;
    float yD = lightPos.y - col.y;

    float dis = sqrt((xD*xD)+(yD*yD) );

    float lV = dis / lightRange;

    lV = 1.0 - lV;

    vec3 diff = vec3( lV,lV,lV  );

    diff = diff * lightDiff;

    vec4 texCol = texture2D(diffTex,gl_TexCoord[0].st);

    if(texCol.a<0.05) discard;

    vec4 fCol = vec4( texCol.r*diff.r,texCol.g*diff.g,texCol.b*diff.b,texCol.a );

    gl_FragColor = fCol * gl_Color;

}