#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "kImage.h"
#include <stdio.h>

class kFrameBuffer
{
public:
	kFrameBuffer(int w, int h);
	void bind();
	void unbind();
	kImage* getBB();

private:
	int fW, fH;
	GLuint id;
	GLuint rb;
	kImage* bb;
	kImage* db;

};

