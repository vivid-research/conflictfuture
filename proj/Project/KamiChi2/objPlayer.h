#pragma once
#include "kObj.h"
#include "objLaserBolt.h"
#include "kScene.h"

class kScene;
class objPlayer : public kObj
{
public:

	objPlayer();
	void update();
	void render();
	void renderLit();
	void init();
	
	void left() {

		

	}

	void right() {

		

	}

	void up() {


	}

	void action() {

		int ct = (int)GetTickCount();

		if ((ct - lastShot) > 150) {

			lastShot = ct;
		
			
			objLaserBolt* lb = new objLaserBolt(getGame());
			lb->setNoIntertia(true);
			lb->setGame(getGame());
			lb->setPos(getX() + getW()+10, getY() + getH()/2, 0);
			
			kLight* lasL = new kLight();

			lasL->setRange(500);
			lasL->setColor(1, 0.2f, 0.2f);
			lasL->setPos(getX() + 178, getY() + 45, 0);
			lasL->setTrack(lb);

			getOwner()->addLight(lasL);
			
			getOwner()->addObj(lb);


		}
	};

	void down() {


	}

	void noaction() {

		
	}

private:

	kImage* idle;
	int lastShot = 0;

};

