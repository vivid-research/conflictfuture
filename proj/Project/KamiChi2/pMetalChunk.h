#pragma once
#include "kParticleSys.h"

class pMetalChunk : public kParticle
{
public:
	static kImage* cImg;
	pMetalChunk() {

	

	}

	pMetalChunk(kamGame * ga) {

		game = ga;

		if (cImg == NULL) {


			cImg = new kImage("data/img/obj/debris/metalChunk1/chunk1-game.png");
			printf("!!!!!!!!!!!!!!!\n");
		}

		float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		float r2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		if (r < 0.5f) r = 0.3f;
		if (r2 < 0.5f) r2 = 0.3f;
		float r3 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		
		if (r3 < 0.4f) r3 = 0.4f;
		angi = -40 + r3 * 80;


		xi = -18.0f + r * 36.0f;
		yi = -18.0f + r2 * 36.0f;
		w = 32;
		h = 32;
		drag = 1.0f;
		x = 0;
		y = 0;
		z = 0;
		
		zi = 0;
		ang = 0;
		//angi = 0;
		r = 1;
		g = 1;
		b = 1;
		a = 1;
		life = 1.0f;

	}

	void update() {
		
		updatePhysics();
		if (yi < 9) {
			yi = yi + 0.1f;
		}

	}

	void render() {

		game->drawImg(x, y, 32, 32, 1, 1, 1, 1, cImg,1.0f,ang);

	}

};

