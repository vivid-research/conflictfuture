#include "kamGame.h"


kamGame::kamGame(kamCore* c) {

	core = c;
	engine = createIrrKlangDevice();
	setBlend(BLENDMODE::Additive);
	printf("Game Created/n");
	musicPlaying = false;

}

void kamGame::setBlend(BLENDMODE mode) {

	bMode = mode;

}

void setBlendGL(BLENDMODE mode) {

	switch (mode) {
	case BLENDMODE::Solid:
		glDisable(GL_BLEND);
		break;
	case BLENDMODE::Alpha:
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case BLENDMODE::Additive:
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
		break;
	}

}

float tX(float x,float y, float rot) {

	x = cos(rot) * x - sin(rot) * y;
	//y = sin(rot) * y;
	return x;
}

float tY(float x, float y, float rot) {
		
	y = sin(rot) * x + cos(rot) * y;
	return y;

}



void kamGame::drawImg(int x, int y, int w, int h, float r, float g, float b, float a, kImage* img, float scale, float  rot) {
	
	
	float pi = 3.14;



	float ra = rot * pi / 180.0f;

	float x1, y1, x2, y2, x3, y3, x4, y4;

	float mx = x + w / 2;
	float my = y + h / 2;

	x1 = x - mx;
	y1 = y - my;
	x2 = (x + w) - mx;
	y2 = (y - my);
	x3 = x2;
	y3 = (y + h) - my;
	x4 = x1;
	y4 = y3;

	float rx1, ry1, rx2, ry2, rx3, ry3, rx4, ry4;

	rx1 = tX(x1, y1, ra);
	ry1 = tY(x1, y1, ra);
	rx2 = tX(x2, y2, ra);
	ry2 = tY(x2, y2, ra);
	rx3 = tX(x3, y3, ra);
	ry3 = tY(x3, y3, ra);
	rx4 = tX(x4, y4, ra);
	ry4 = tY(x4, y4, ra);


	rx1 = rx1 * scale;
	ry1 = ry1 * scale;
	rx2 = rx2 * scale;
	ry2 = ry2 * scale;
	rx3 = rx3 * scale;
	ry3 = ry3 * scale;
	rx4 = rx4 * scale;
	ry4 = ry4 * scale;

	setBlendGL(bMode);

	img->bind(0);

	glColor4f(r, g, b, a);

	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(mx + rx1, my + ry1);
	glTexCoord2f(1, 0);
	glVertex2f(mx + rx2, my + ry2);
	glTexCoord2f(1, 1);
	glVertex2f(mx + rx3, my + ry3);
	glTexCoord2f(0, 1);
	glVertex2f(mx + rx4, my + ry4);

	glEnd();

	img->release(0);

}

void kamGame::drawImg(int x, int y, int w, int h, float r, float g, float b, float a, kImage* img,float scale)
{

	float x1, y1, x2, y2, x3, y3, x4, y4;

	float mx = x + w / 2;
	float my = y + h / 2;

	x1 = x - mx;
	y1 = y - my;
	x2 = (x + w) - mx;
	y2 = (y - my);
	x3 = x2;
	y3 = (y + h) - my;
	x4 = x1;
	y4 = y3;

	x1 = x1 * scale;
	y1 = y1 * scale;
	x2 = x2 * scale;
	y2 = y2 * scale;
	x3 = x3 * scale;
	y3 = y3 * scale;
	x4 = x4 * scale;
	y4 = y4 * scale;

	setBlendGL(bMode);

	img->bind(0);

	glColor4f(r, g, b, a);

	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(mx+x1, my+y1);
	glTexCoord2f(1, 0);
	glVertex2f(mx+x2, my+y2);
	glTexCoord2f(1, 1);
	glVertex2f(mx+x3,my+y3);
	glTexCoord2f(0, 1);
	glVertex2f(mx+x4,my+y4);

	glEnd();

	img->release(0);

}

void kamGame::drawImg(int x, int y, int w, int h, float r, float g, float b, float a, kImage* img)
{

	setBlendGL(bMode);

	img->bind(0);

	glColor4f(r, g, b, a);

	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(x, y);
	glTexCoord2f(1, 0);
	glVertex2f(x + w, y);
	glTexCoord2f(1, 1);
	glVertex2f(x + w, y + h);
	glTexCoord2f(0, 1);
	glVertex2f(x, y + h);

	glEnd();

	img->release(0);

}

void kamGame::drawRect(int x, int y, int w, int h, float r, float g, float b, float a) {

	//glLoadMatrixf()

	setBlendGL(bMode);

	glColor4f(r, g, b, a);

	glBegin(GL_QUADS);

	glVertex2f(x,y);
	glVertex2f(x + w, y);
	glVertex2f(x + w, y + h);
	glVertex2f(x, y + h);

	glEnd();


}

void kamGame::drawLine(int x, int y, int x1, int y2, float w, float r, float g, float b, float a) {

	setBlendGL(bMode);

	glColor4f(r, g, b, a);

	float ang = atan2(y2 - y, x1 - x);


	glBegin(GL_QUADS);

	glVertex2f(x, y);
	glVertex2f(x + w, y);
	glVertex2f(x1+w, y2);
	glVertex2f(x1, y2);


	glEnd();

}

void kamGame::playMusic(const char* path) {

	musicPlaying = true;
	music = engine->play2D(path, true, false, true);

}

void kamGame::stopMusic() {

	if (musicPlaying) {
		music->stop();
		musicPlaying = false;
	}

}

kSoundSource * kamGame::loadSound(const char* path) {

	return new kSoundSource(engine->addSoundSourceFromFile(path, ESM_AUTO_DETECT, true));

}

kSound* kamGame::playSound(kSoundSource* src,bool loop) {

	return new kSound(engine->play2D(src->getSource(), loop,false,true));


}

int kamGame::getW() {

	return core->getW();

}

int kamGame::getH() {

	return core->getH();

}