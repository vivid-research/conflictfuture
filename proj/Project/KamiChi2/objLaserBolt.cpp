#include "objLaserBolt.h"

kSoundSource* objLaserBolt::lasSrc = NULL;
kImage* objLaserBolt::imgSrc = NULL;

objLaserBolt::objLaserBolt(kamGame * g) {


	setGame(g);
	if (imgSrc == NULL) {
		imgSrc = new kImage("data/img/obj/laserOne/bolt.png");
		lasSrc = getGame()->loadSound("data/sfx/obj/laserbolt/bolt1.wav");
	}

	boltImg = imgSrc;
	getGame()->playSound(lasSrc, false);



	setDrag(1.0f);
	setSize(64, 32);
	setCollider(true);


}