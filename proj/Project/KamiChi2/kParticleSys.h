#pragma once
#include "kamGame.h"
#include <vector>

class kParticle {
public:
	kParticle() {

	}
	float x, y, z;
	float xi, yi, zi;
	float ang;
	float angi;
	float drag;
	virtual void update() {};
	virtual void render() {};
	 void updatePhysics()
	{
		
		 ang += angi;
		x = x + xi;
		y = y + yi;

	};
	float r, g, b, a;
	float life;
	float w, h;
	kamGame* game;
};
class kParticleSys
{
public:
	kParticleSys(kamGame * g) {
		pool.resize(0);
		game = g;
	}

	void addPart(kParticle* p)
	{

		pool.push_back(p);

	}

	void update() {

		for (int i = 0; i < pool.size(); i++) {

			pool[i]->update();

		}

	}

	void render() {

		for (int i = 0; i < pool.size(); i++) {

			pool[i]->render();

		}

	}

private:
	std::vector<kParticle*> pool;
	kamGame* game;
};


