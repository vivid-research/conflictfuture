#pragma once
#include <vector>

class kPathPoint {
public:
	float x, y;
	float spd;
	kPathPoint(float px, float py, float s) {

		x = px;
		y = py;
		spd = s;

	}

};
class kPath
{
public:

	kPath() {
		points.resize(0);
	}

	void addPoint(kPathPoint* p)
	{

		points.push_back(p);

	}

	std::vector<kPathPoint*> points;
};

