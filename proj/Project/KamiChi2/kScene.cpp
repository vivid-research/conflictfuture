#include "kScene.h"



kScene::kScene(kamCore *c,kamGame* g) {

	//kGlobal::curScene = this;
	objs.resize(0);
	lights.resize(0);
	background = NULL;
	game = g;
	core = c;
	fxLight = new kEffect("data/shader/light/lightvertex.glsl", "data/shader/light/lightfrag.glsl");

	sceneFrame = new kFrameBuffer(g->getW(), g->getH());

	stars = new kamStars(g);
	stars->initStars(150);

	pBloom = new ppBloom();
	pBloom->setGame(game);
	pSys = new kParticleSys(g);
	pDeform = new ppDeformer(game);
	sDeform = new kImage("data/img/fx/deformer/swave2.png");
	waves.resize(0);

}

void kScene::addPart(kParticle* p) {

	pSys->addPart(p);

}

void kScene::addObj(kObj* obj) {

	objs.push_back(obj);
	obj->setOwner(this);

};

void kScene::update() {

	for (int i = 0; i < waves.size(); i++) {

		waves[i]->size = waves[i]->size + 75.0f;
		waves[i]->life = waves[i]->life * 0.96f;
		
	}
		
	for (int i = 0; i < objs.size(); i++) {

		for (int i2 = 0; i2 < objs.size(); i2++) {


			if (i != i2) {

				if (objs[i]->getCollider() == true) {

					float x1, y1;

					x1 = objs[i]->getX() + objs[i]->getW() / 2;
					y1 = objs[i]->getY() + objs[i]->getH() / 2;

					float x2, y2, w2, h2;

					x2 = objs[i2]->getX();
					y2 = objs[i2]->getY();
					w2 = objs[i2]->getW();
					h2 = objs[i2]->getH();

					if (x1 > x2&& x1 < (x2 + w2)) {

						if (y1 > y2&& y1 < (y2 + h2)) {

							objs[i2]->collidedWith(objs[i]);

						}

					}

				}

			}

		}

	}

	stars->update(7);

	for (int i = 0; i < lights.size(); i++) {

		lights[i]->updateTrack();

	}
	for (int i = 0; i < objs.size(); i++) {

		objs[i]->internalUpdate();
		objs[i]->update();
		objs[i]->updateAnim();

	}

	pSys->update();

};

void kScene::renderLit() {

	sceneFrame->bind();

	

	if (background != NULL) {

		game->drawImg(0, 0, game->getW(), game->getH(), 1, 1, 1, 1, background);

	}
	stars->render();
	fxLight->bind();

	fxLight->setInt("diffTex", 0);



		
	for (int o = 0; o < objs.size(); o++) {


		game->setBlend(BLENDMODE::Alpha);
		for (int i = 0; i < lights.size(); i++) {

			fxLight->setVec3("lightPos", lights[i]->getX(), lights[i]->getY(), 0);
			fxLight->setFloat("lightRange", lights[i]->getRange());
			fxLight->setVec3("lightDiff", lights[i]->getDifR(), lights[i]->getDifG(), lights[i]->getDifB());

			objs[o]->render();
			game->setBlend(BLENDMODE::Additive);

		}




	}

	fxLight->unbind();

	for (int i = 0; i < objs.size(); i++) {

		objs[i]->postRender();

	}

	pSys->render();

	sceneFrame->unbind();

	game->setBlend(BLENDMODE::Solid);

	kImage * fs = pBloom->process(sceneFrame->getBB());

	//game->drawImg(0, game->getH(),game->getW(), -game->getH(), 1, 1, 1, 1, fs);



	//return;

	pDeform->beginDeform();
	game->setBlend(BLENDMODE::Alpha);
	//pDeform->paint(sDeform, 0, 0, game->getW(), game->getH());
	//pDeform->paint(sDeform, 200, 200, 600, 600);
	for (int i = 0; i < waves.size(); i++) {

		if (waves[i]->life > 0.1f) {
			pDeform->paint(sDeform, waves[i]->x, game->getH() - waves[i]->y, waves[i]->size, waves[i]->size);
		}
	}
	pDeform->endDeform();

	game->setBlend(BLENDMODE::Solid);

	fs=pDeform->process(fs);

	game->drawImg(0,0, game->getW(),  game->getH(), 1, 1, 1, 1, fs);



	//game->drawImg(0, 0, game->getW(), game->getH(), 1, 1, 1, 1, pDeform->getBB());



//	game->drawImg(0, 0, 256, 256, 1, 1, 1, 1, sceneFrame->getBB());

}

void kScene::render() {

	if (background != NULL) {

		game->drawImg(0, 0, game->getW(), game->getH(), 1, 1, 1, 1, background);

	}

	for (int i = 0; i < objs.size(); i++) {

		objs[i]->render();

	}

};

void kScene::setBg(kImage* bg) {

	background = bg;

}

void kScene::clearScene() {

	objs.clear();
	lights.clear();

	objs.resize(0);
	lights.resize(0);

}

kObj* kScene::getObj(int num) {


	return objs[num];

}

int kScene::numObjs() {

	return objs.size();

};

void kScene::addLight(kLight* l) {

	lights.push_back(l);

}

kLight* kScene::getLight(int num) {

	return lights[num];

}

int kScene::numLights() {

	return lights.size();

}