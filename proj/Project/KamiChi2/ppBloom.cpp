#include "ppBloom.h"

ppBloom::ppBloom() {

	getLight = new kEffect("data/shader/pp/bloom/getLightVertex.glsl", "data/shader/pp/bloom/getLightFrag.glsl");
	blurImg = new kEffect("data/shader/pp/bloom/blurImgVertex.glsl", "data/shader/pp/bloom/blurImgFrag.glsl");
	finalBloom = new kEffect("data/shader/pp/bloom/finalBloomVertex.glsl", "data/shader/pp/bloom/finalBloomFrag.glsl");

}

kImage* ppBloom::process(kImage *base) {

	if (fb1 == NULL) {

		fb1 = new kFrameBuffer(getGame()->getW(), getGame()->getH());
		fb2 = new kFrameBuffer(getGame()->getW(), getGame()->getH());
	}

	fb1->bind();

	getLight->bind();

	getLight->setInt("fb", 0);
	getLight->setFloat("tH", 0.55f);

	getGame()->drawImg(0, 0, getGame()->getW(), getGame()->getH(), 1, 1, 1, 1, base);

	getLight->unbind();

	fb1->unbind();

	fb2->bind();

	blurImg->bind();

	blurImg->setInt("fb", 0);

	blurImg->setFloat("blur", 0.5f);

	getGame()->drawImg(0, 0, getGame()->getW(), getGame()->getH(), 1, 1, 1, 1, fb1->getBB());

	blurImg->unbind();

	fb2->unbind();

	fb1->bind();

	finalBloom->bind();

	finalBloom->setInt("fb", 0);
	finalBloom->setInt("blur", 1);

	
	fb2->getBB()->bind(1);

	getGame()->drawImg(0, 0, getGame()->getW(), getGame()->getH(), 1, 1, 1, 1,base);

	fb2->getBB()->release(1);

	finalBloom->unbind();

	fb1->unbind();

	return fb1->getBB();

	//getGame()->drawImg(0, 0, getGame()->getW()/2, getGame()->getH()/2, 1, 1, 1, 1,fb2->getBB());



}