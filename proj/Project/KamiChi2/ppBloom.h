#pragma once
#include "kPostProcess.h"
#include "kFrameBuffer.h"

class ppBloom : public kPostProcess
{
public:

	ppBloom();

	kImage* process(kImage * base);

private:
	kEffect* getLight;
	kEffect* blurImg;
	kEffect* finalBloom;
	kFrameBuffer* fb1 = NULL;
	kFrameBuffer* fb2 = NULL;

};

