#pragma once
#include "kObj.h"
#include "kElectricFX.h"
#include "kSCene.h"
//#include "kParticleSys.h"

#include "pMetalChunk.h"
class objShipMine : public kObj
{
public:

	objShipMine();
	void render();
	void update() {

		setAng(getAng() + 0.5f);
		if (sparks != NULL) {
		
			sparks->update();

		}
		
		followPath();

	}

	void loadAssets() {
		 
		hitSrc = getGame()->loadSound("data/sfx/obj/weaponHit/hit1.wav");
		expSrc = getGame()->loadSound("data/sfx/obj/explode/explode1.wav");
	}

	void collidedWith(kObj* w) {

		if (getHp() <= 0) return;

		int time = (int)GetTickCount();

		if (time > (getHit() + 150))
		{
			getOwner()->addWave(getX()+getW()/2, getY()+getH()/2, 40, 2.0f);
			float cs = w->getColliderStr();
			
			float hp = getHp();

			hp = hp - cs;
			if (hp <= 0)
			{
				getGame()->playSound(expSrc, false);
				sparks->SetPos(getX() + getW() / 2, getY() + getH() / 2);
				sparks->action(80);
				hp = 0;
				for (int i = 0; i < 15; i++) {
					pMetalChunk* tp = new pMetalChunk(getGame());
					tp->x = getX() + getW() / 2;
					tp->y = getY() + getH() / 2;
					getOwner()->addPart(tp);
				}
		


			}

			setHp(hp);


			justHit();
			if (hitSrc == NULL) {



			}
			getGame()->playSound(hitSrc, false);
			
			move(5, 0);
			if (sparks == NULL) {
				sparks = new kElectricFX(getGame());
			}
				sparks->SetPos(getX()+getW()/2, getY()+getH()/2);
				sparks->action(18);

				
			
		}
	};

	void postRender();

private:


	kImage* mine;
	kElectricFX* sparks = NULL;
	kSoundSource* hitSrc;
	kSoundSource* expSrc;

};

