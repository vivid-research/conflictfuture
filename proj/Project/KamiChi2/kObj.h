#pragma once
#include "kamGame.h"
#include "kAnimSet.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "kPath.h"
template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}
class kScene;

class kObj
{

public:

	kObj();

	void setGame(kamGame* g)
	{
		game = g;
	};

	kamGame* getGame() {

		return game;

	}

	void followPath() {
		
		kPath * path = getCurPath();

		if (path == NULL) return;


		if (pathPoint >= path->points.size()) {
			return;
		}

		kPathPoint* tar = path->points[pathPoint];

		float xd = tar->x - getX();
		float yd = tar->y - getY();

		float dis = sqrt((xd * xd) + (yd * yd));


		float dx = xd / dis;  //sgn(xd) * tar->spd;
		float dy = yd / dis;

		dx = dx * tar->spd;
		dy = dy * tar->spd;

		move(dx, dy);


		if (dis < 25) {

			pathPoint++;

		}

	}
	virtual void update() {};
	virtual void render() {};
	virtual void renderLit() {};
	virtual void init() {};
	virtual void done() {};
	virtual void action() {};
	virtual void left() {};
	virtual void right() {};
	virtual void up() {};
	virtual void down() {}
	virtual void noaction() {};

	float getW() {

		return w;

	};

	float getH() {

		return h;

	};

	void updateAnim() {

		if (curAnim == NULL) return;
		curAnim->updateSet();

	}


	void setAnim(kAnimSet* set,float speed)
	{
		
		curAnim = set;
		set->reset();
		set->setSpeed(speed);

		//curFrame = 0;
		//animSpeed = speed;
		//animDir = true;

	};

	void renderFrame();

	void setSize(int ow, int oh) {

		w = ow;
		h = oh;

	}

	

	void setPos(int ox, int oy, int oz) {

		x = ox;
		y = oy;
		z = oz;

		

	}

	void move(float mx, float my) {

		if (noInertia) {
		
			x += mx;
			y += my;

		}
		else {
			xi += mx;
			yi += my;
		}
	}

	void internalUpdate() {

		x += xi;
		y += yi;

		xi *= drag;
		yi *= drag;

	}

	kAnimSet* getCurAnim() {

		return curAnim;

	};

	void setOwner(kScene* own)
	{
		owner = own;

	};

	kScene* getOwner() {

		return owner;

	};

	void setDrag(float d);

	float getX() { return x; };
	float getY() { return y; };

	void setTrack(kObj* o);

	void updateTrack() {

		if (track != NULL) {

			x = track->getX();
			y = track->getY();

		}

	}

	void setNoIntertia(bool v) {

		noInertia = v;

	}

	virtual void collidedWith(kObj* w) {};



	

	void setCollider(bool c) {

		collider = c;
	}

	bool getCollider() {

		return collider;

	}

	void justHit() {

		lastHit = (int)GetTickCount();

	};

	int getHit() {

		return lastHit;

	}


	virtual void loadAssets() {};
	virtual void postRender() {};
	float getColliderStr() {

		return collideStr;

	};

	void setHp(float h) {

		hp = h;

	};

	float getHp() {

		return hp;

	};

	float getAng() {

		return ang;

	}

	void setAng(float a) {

		ang = a;

	}

	kPath* getPath(int num) {

		return paths[num];

	};

	void addPath(kPath* p) {

		paths.push_back(p);

	}

	void setPath(int num) {
		
		curPath = paths[num];

	}

	kPath* getCurPath() {

		return curPath;

	}

private:

	const char* name;
	float x, y, z;
	float xi, yi, zi;
	bool noInertia = false;
	float drag = 0.90f;
	float w, h;
	kAnimSet* curAnim;
	int curFrame = 0;
	float animTime = 0.0f;
	float animSpeed = 0.1f;
	bool animDir = 0;
	kamGame* game;
	kScene* owner;
	kObj* track;
	bool collider = false;
	int lastHit = 0;
	float hp = 1.0f;
	float collideStr = 0.15f;
	float ang = 0;
	std::vector<kPath*> paths;
	kPath* curPath = NULL;
	int pathPoint = 0;
	bool pointReached = false;
};

