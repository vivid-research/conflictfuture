#pragma once
#include "kamState.h"
#include "kamStars.h"
//#include "kObj.h";
#include "kScene.h"
#include "objPlayer.h"
#include <vector>

class stateNormalMode : public kamState
{
public:

	stateNormalMode(kamCore * c,kamGame* g);
	void setMusic(const char* m) {

		music = m;
		hasMusic = true;


	}
	void init();
	void update();
	void render();
	void done();
	void pause();
	void resume();
	void addObj(kObj* obj);
private:

	kamStars* stars; 
	//std::vector<kObj*> objs;
	
	kScene* scene;
	
	objPlayer* player;
	const char* music = "";
	bool hasMusic = false;
	bool musicStarted = false;


};

