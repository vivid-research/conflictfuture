#include "levTheWarBegins.h"

void levTheWarBegins::init() {

	beginState(0);
	

}

void levTheWarBegins::update() {


	
}

void levTheWarBegins::render() {

}

void levTheWarBegins::done() {


}

void levTheWarBegins::up() {

	player->move(0, -1.3f);
	player->up();

}

void levTheWarBegins::down() {

	player->move(0, 1.3f);
	player->down();
}

void levTheWarBegins::left() {

	player->move(-1.3f,0);
	player->left();
}

void levTheWarBegins::right() {

	
	player->move(1.3f, 0);
	player->right();

}

void levTheWarBegins::action() {

	player->action();

};

void levTheWarBegins::nomove() {

	player->noaction();

}

levTheWarBegins::levTheWarBegins(kamCore * c,kamGame *g)
{

	setGame(g);

	setLevel("The War Begins/n", 2);

	stateLevelIntro* intro = new stateLevelIntro(g,getLevelName());
	stateNormalMode* normal = new stateNormalMode(c,g);
	normal->setMusic("data/sfx/song/level/one/bgMusic.mp3");

	objShipMine* mine1 = new objShipMine();
	mine1->setGame(g);
	mine1->setPos(1200, 250,1);
	mine1->loadAssets();

	kPath* path1 = new kPath();

	kPathPoint* p1 = new kPathPoint(900, 450, 0.3f);
	kPathPoint* p2 = new kPathPoint(600, 10, 0.3f);
	kPathPoint* p3 = new kPathPoint(700, 600, 0.3f);
	kPathPoint* p4 = new kPathPoint(300, 200, 0.5f);
	kPathPoint* p5 = new kPathPoint(-100, 300, 0.7f);


	path1->addPoint(p1);
	path1->addPoint(p2);
	path1->addPoint(p3);
	path1->addPoint(p4);
	path1->addPoint(p5);


	mine1->addPath(path1);
	mine1->setPath(0);

	objPlayer* play = new objPlayer();

	play->setGame(g);

	player = play;

	normal->addObj(play);
	normal->addObj(mine1);

	play->setPos(64, 400,1);
	
	setState(intro, 0);
	setState(normal, 1);


}