#pragma once
#include "kamGame.h"
#include <vector>

class kElectricLine {

public:

	kElectricLine(float px, float py, float px2, float py2, float xlit) {

		x1 = px;
		y1 = py;
		x2 = px2;
		y2 = py2;
		lit = xlit;

	}



	float x1, y1, x2, y2;
	float lit = 1.0f;
	bool repeat = false;
	int num = 0;

};
class kElectricFX
{
public:

	kElectricFX(kamGame *g);

	void SetPos(float px, float py) {

		x = px;
		y = py;

	}

	void render() {

		game->setBlend(BLENDMODE::Alpha);

		for (int i = 0; i < lines.size(); i++) {

			game->drawLine(lines[i]->x1, lines[i]->y1, lines[i]->x2, lines[i]->y2, (int)(3+15.0f*lines[i]->lit), 0.1f, 1.4f, 1.4f, 1*lines[i]->lit);

		}

	}

	void action(int num) {

		for (int i = 0; i < num; i++) {

		
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

			float r2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

			float r3 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);


			r = r * 90;

			r2 = r2 * 90;


			kElectricLine* line = new kElectricLine(x, y,x-45+r, y-45+r2,r3);

			lines.push_back(line);

		}

	}

	void update() {

		for (int i = 0; i < lines.size(); i++) {
			//exit(1);
			lines[i]->lit *= 0.8f;

		
				if (!lines[i]->repeat && lines[i]->num<3 && lines[i]->lit<0.5f) {

					float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

					float r2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

					float r3 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

					r = r * 55;

					r2 = r2 * 55;

					float xd, yd;

					xd = lines[i]->x2 - lines[i]->x1;
					yd = lines[i]->y2 - lines[i]->y1;


					kElectricLine* line = new kElectricLine(lines[i]->x2, lines[i]->y2, lines[i]->x2 + xd+(-26+r), lines[i]->y2 + yd + (-26+r2),r3);

					lines[i]->repeat = true;

				
					line->num = lines[i]->num + 1;

					lines.push_back(line);

				


			}

		}

	}

private:
	float x=0;
	float y = 0;
	kamGame* game;
	std::vector<kElectricLine *> lines;
};



