#pragma once
#include "kEffect.h"
#include "kImage.h"
#include "kamGame.h"

class kPostProcess
{
public:

	kPostProcess();

	void setGame(kamGame* g);

	kamGame* getGame();

	virtual kImage* process(kImage* base) { return NULL; };


private:

	kamGame* game;

};

