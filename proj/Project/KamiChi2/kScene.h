#pragma once
#include "kObj.h"
#include "kLight.h"
#include <vector>
#include "kImage.h"
#include "kamGame.h"
#include "kamCore.h"
#include "kEffect.h"
#include "kFrameBuffer.h"
#include "kamStars.h"
#include "ppBloom.h"
#include "ppDeformer.h"
#include "kParticleSys.h"

class kDeformWave {
public:
	float x, y;
	float size;
	float life = 1.0f;
	kDeformWave(float dx, float dy, float dsize, float dlife)
	{

		x = dx;
		y = dy;
		size = dsize;
		life = dlife;

	};

};

class kScene
{
public:


	kScene(kamCore *c,kamGame* g);
	void addObj(kObj* obj);
	void update();
	void render();
	void setBg(kImage* bg);
	
	int numObjs();

	kObj* getObj(int num);
	void addLight(kLight* l);
	kLight* getLight(int num);
	int numLights();


	

	void clearScene();

	void renderLit();

	void addPart(kParticle* p);

	void addWave(float x, float y, float size, float life)
	{

		kDeformWave* wave = new kDeformWave(x,y,size,life);
	
		waves.push_back(wave);



	}
private:

	std::vector<kObj*> objs;
	std::vector<kLight*> lights;
	kImage* background;
	kamGame* game;
	kamCore* core;
	kEffect* fxLight;
	kFrameBuffer* sceneFrame;
	kamStars* stars;
	ppBloom* pBloom;
	kParticleSys* pSys;
	ppDeformer* pDeform;
	kImage* sDeform;
	std::vector<kDeformWave*> waves;


};

