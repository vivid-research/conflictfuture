#include "pathWave.bmx.gui.debug.win32.x64.h"
static BBString _s1={
	&bbStringClass,
	8,
	{78,101,119,32,87,97,118,101}
};
static BBString _s0={
	&bbStringClass,
	35,
	{78,111,32,115,112,97,99,101,32,116,111,32,105,110,99,114,101
	,97,115,101,32,100,101,113,117,101,32,99,97,112,97,99,105
	,116,121}
};
struct BBDebugScope_5{int kind; const char *name; BBDebugDecl decls[6]; };
BBINT _m_pathWave_button(BBINT bbt_x,BBINT bbt_y,BBINT bbt_w,BBINT bbt_h,BBSTRING bbt_name){
	struct BBDebugScope_5 __scope = {
		BBDEBUGSCOPE_FUNCTION,
		"button",
		{
			{
				BBDEBUGDECL_LOCAL,
				"x",
				"i",
				.var_address=&bbt_x
			},
			{
				BBDEBUGDECL_LOCAL,
				"y",
				"i",
				.var_address=&bbt_y
			},
			{
				BBDEBUGDECL_LOCAL,
				"w",
				"i",
				.var_address=&bbt_w
			},
			{
				BBDEBUGDECL_LOCAL,
				"h",
				"i",
				.var_address=&bbt_h
			},
			{
				BBDEBUGDECL_LOCAL,
				"name",
				"$",
				.var_address=&bbt_name
			},
			BBDEBUGDECL_END 
		}
	};
	bbOnDebugEnterScope(&__scope);
	struct BBDebugStm __stmt_0 = {"C:/Projects/KamiChi2/Tools/pathWave/toolUI.bmx", 4, 0};
	bbOnDebugEnterStm(&__stmt_0);
	brl_max2d_DrawRect(((BBFLOAT)bbt_x),((BBFLOAT)bbt_y),((BBFLOAT)bbt_w),((BBFLOAT)bbt_h));
	struct BBDebugStm __stmt_1 = {"C:/Projects/KamiChi2/Tools/pathWave/toolUI.bmx", 5, 0};
	bbOnDebugEnterStm(&__stmt_1);
	brl_max2d_DrawText(bbt_name,((BBFLOAT)((bbt_x+(bbt_w/2))-(brl_max2d_TextWidth(bbt_name)/2))),((BBFLOAT)((bbt_y+(bbt_h/2))-brl_max2d_TextHeight(bbt_name))));
	bbOnDebugLeaveScope();
	return 0;
}
static int _bb_main_inited = 0;
int _bb_main(){
	if (!_bb_main_inited) {
		_bb_main_inited = 1;
		__bb_brl_blitz_blitz();
		__bb_brl_appstub_appstub();
		__bb_brl_audio_audio();
		__bb_brl_bank_bank();
		__bb_brl_bankstream_bankstream();
		__bb_brl_base64_base64();
		__bb_brl_basic_basic();
		__bb_brl_bmploader_bmploader();
		__bb_brl_clipboard_clipboard();
		__bb_brl_collections_collections();
		__bb_brl_crypto_crypto();
		__bb_brl_d3d7max2d_d3d7max2d();
		__bb_brl_d3d9max2d_d3d9max2d();
		__bb_brl_data_data();
		__bb_brl_directsoundaudio_directsoundaudio();
		__bb_brl_eventqueue_eventqueue();
		__bb_brl_freeaudioaudio_freeaudioaudio();
		__bb_brl_freetypefont_freetypefont();
		__bb_brl_glgraphics_glgraphics();
		__bb_brl_glmax2d_glmax2d();
		__bb_brl_gnet_gnet();
		__bb_brl_jpgloader_jpgloader();
		__bb_brl_json_json();
		__bb_brl_map_map();
		__bb_brl_matrix_matrix();
		__bb_brl_maxlua_maxlua();
		__bb_brl_maxutil_maxutil();
		__bb_brl_oggloader_oggloader();
		__bb_brl_openalaudio_openalaudio();
		__bb_brl_pngloader_pngloader();
		__bb_brl_quaternion_quaternion();
		__bb_brl_retro_retro();
		__bb_brl_tgaloader_tgaloader();
		__bb_brl_threadpool_threadpool();
		__bb_brl_timer_timer();
		__bb_brl_timerdefault_timerdefault();
		__bb_brl_volumes_volumes();
		__bb_brl_wavloader_wavloader();
		__bb_brl_xml_xml();
		__bb_pub_freejoy_freejoy();
		__bb_pub_freeprocess_freeprocess();
		__bb_pub_glad_glad();
		__bb_pub_macos_macos();
		__bb_pub_nx_nx();
		__bb_pub_opengles_opengles();
		__bb_pub_vulkan_vulkan();
		__bb_pub_xmmintrin_xmmintrin();
		struct BBDebugScope __scope = {
			BBDEBUGSCOPE_FUNCTION,
			"pathWave",
			{
				BBDEBUGDECL_END 
			}
		};
		bbOnDebugEnterScope(&__scope);
		struct BBDebugStm __stmt_0 = {"C:/Projects/KamiChi2/Tools/pathWave/pathWave.bmx", 4, 0};
		bbOnDebugEnterStm(&__stmt_0);
		brl_graphics_Graphics(1378,768,0,60,0);
		struct BBDebugStm __stmt_1 = {"C:/Projects/KamiChi2/Tools/pathWave/pathWave.bmx", 6, 0};
		bbOnDebugEnterStm(&__stmt_1);
		while(1){
			struct BBDebugScope __scope = {
				BBDEBUGSCOPE_LOCALBLOCK,
				0,
				{
					BBDEBUGDECL_END 
				}
			};
			bbOnDebugEnterScope(&__scope);
			struct BBDebugStm __stmt_0 = {"C:/Projects/KamiChi2/Tools/pathWave/pathWave.bmx", 8, 0};
			bbOnDebugEnterStm(&__stmt_0);
			brl_max2d_Cls();
			struct BBDebugStm __stmt_1 = {"C:/Projects/KamiChi2/Tools/pathWave/pathWave.bmx", 10, 0};
			bbOnDebugEnterStm(&__stmt_1);
			if(_m_pathWave_button(10,5,120,30,&_s1)!=0){
				struct BBDebugScope __scope = {
					BBDEBUGSCOPE_LOCALBLOCK,
					0,
					{
						BBDEBUGDECL_END 
					}
				};
				bbOnDebugEnterScope(&__scope);
				bbOnDebugLeaveScope();
			}
			struct BBDebugStm __stmt_2 = {"C:/Projects/KamiChi2/Tools/pathWave/pathWave.bmx", 15, 0};
			bbOnDebugEnterStm(&__stmt_2);
			brl_graphics_Flip(-1);
			bbOnDebugLeaveScope();
		}
	}
	return 0;
}