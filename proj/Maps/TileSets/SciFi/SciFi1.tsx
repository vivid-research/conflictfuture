<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.0" name="SciFi1" tilewidth="64" tileheight="64" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="Floor">
  <image width="64" height="64" source="MetalPlate1.png"/>
 </tile>
 <tile id="1" type="Floor">
  <image width="64" height="64" source="MetalPlate2.png"/>
 </tile>
 <tile id="2" type="Floor">
  <image width="64" height="64" source="MetalPlate3.png"/>
 </tile>
 <tile id="3" type="Floor">
  <image width="64" height="64" source="MetalPlate4.png"/>
 </tile>
</tileset>
